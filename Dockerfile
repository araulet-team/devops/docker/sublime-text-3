FROM ubuntu:latest

# Will not prompt for questions
ARG DEBIAN_FRONTEND=noninteractive

ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

RUN echo "===> install dependencies" && \
	apt-get update && apt-get -y install \
		apt-transport-https \
        apt-utils \
		wget \
		gnupg \
		locales \
        ca-certificates \
        dbus-x11 \
        python \
        libcanberra-gtk-module \
        libgtk2.0-0 \
        libatk-adaptor \
        libgail-common

RUN locale-gen en_US.UTF-8

# classic install from the documentation
# https://www.sublimetext.com/docs/3/linux_repositories.html
RUN ["/bin/bash", "-c", "set -o pipefail && wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -"]
RUN ["/bin/bash", "-c", "set -o pipefail && echo \"deb https://download.sublimetext.com/ apt/stable/\" | tee /etc/apt/sources.list.d/sublime-text.list"]
RUN apt-get update && apt-get -y install \
	sublime-text

RUN rm -rf /var/lib/apt/lists/*

COPY run.sh /run.sh
RUN chmod +x /run.sh

CMD ["/run.sh"]
